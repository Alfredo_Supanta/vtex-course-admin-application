<h1 align="center">:file_cabinet: Admin Applications</h1>

## :memo: Descripción

Admin Applications esta siendo desarrollado siguiendo las lecciones del Learning Path de Vtex. El bloque cursados para realizar la app es:<b>Admin Applications (block 8)</b>

## :wrench: Tecnologias utilizadas

- <b>Vtex CLI</b>
  <b>React</b>
  <b>GraphQL</b>

## :rocket: Levantar el proyecto

Para rodar o repositório é necessário clonar o mesmo, dar o seguinte comando para iniciar o projeto:

```
<git clone https://gitlab.com/Alfredo_Supanta/vtex-course-admin-application>
<vtex login xxxxxx>
<vtex link>
```

## :handshake: Colaboradores

<table>
  <tr>
    <td align="center">
      <a href="https://github.com/AlfredoMarcelo">
        <img src="https://avatars.githubusercontent.com/u/82328683?s=96&v=4" width="100px;" alt="Foto de Alfredo en GitHub"/><br>
        <sub>
          <b>Alfredo Supanta</b>
        </sub>
      </a>
    </td>
  </tr>
</table>
